#!/bin/bash

function stage1() {

echo "-------------------------------------------------"
echo " Submitting Jobs for base(cross-sectional) stage"
echo "-------------------------------------------------"

# iterate subjects
for subject in $SUBJECTS; do 
  for tp in $(seq 1 $TP_COUNT); do
    curr_suffix=$(eval echo \${TP_SUFFIX_$tp})
    curr_subj=${SUB_PREFIX}${subject}${curr_suffix}
    curr_img=${IMG_PREFIX}${curr_subj}${IMG_SUFFIX}

    echo -ne "\n[subject: ${SUB_PREFIX}${subject} timepoint: '${curr_suffix}'] :\t"

    # check for raw image file
    if [ ! -e ${curr_img} ] ; then
      warn -n "raw image ${curr_img} not found, skipping"
      continue
    fi

    # check for running/queued jobs
    if [ "x$(qselect -N ${JOB_PREFIX}-base-${curr_subj})" != "x" ] ; then
      info -n "stage1 in process, skipping"
      continue
    fi

    # skip, if already done
    logfile=${curr_subj}"/scripts/recon-all.log"
    if [ -e ${logfile} ]; then
      if [ "x$(tail -n1 ${logfile} | grep 'finished without')" != "x" ] ; then
        cheer -n "stage1 already finished without errors"
        continue
      fi
      if [ -d ${curr_subj} ]; then
        warn -n "stage1 present and faulty, resubmitting to try again"
	    rm -rf ${curr_subj}
      fi
    else
        cheer -n "submitting"
    fi

    # meta-data
    echo "#PBS -N ${JOB_PREFIX}-base-${curr_subj}" >> jobfile
    echo "#PBS -l walltime=30:00:00" >> jobfile
    echo "#PBS -m n" >> jobfile

    # actual commands for the job
    echo 'cd $PBS_O_WORKDIR' >> jobfile
    echo "export FREESURFER_HOME=$FREESURFER_HOME" >> jobfile
    echo 'source $FREESURFER_HOME/SetUpFreeSurfer.sh' >> jobfile
    echo "export SUBJECTS_DIR='./'" >> jobfile
    echo "recon-all -all -i ${curr_img} -subjid ${curr_subj}" >> jobfile

    # submit and clean temporary file
    qsub jobfile > /dev/null
    rm jobfile
  done
done

echo -e "\n"    #newline
}
