#!/bin/bash

# generates jobs to create longitudinal jobs and submits them
#  + checks if base and template data is present
#  + checks if longitudinal has already been done


function stage3 {

echo "----------------------------------------"
echo " Submitting Jobs for longitudinal stage"
echo "----------------------------------------"

# iterate subjects
for subject in $SUBJECTS; do
  for tp in $(seq 1 $TP_COUNT); do
    curr_suffix=$(eval echo \${TP_SUFFIX_$tp})
    curr_subj=${SUB_PREFIX}${subject}${curr_suffix}
    echo -ne "\n[subject: ${SUB_PREFIX}${subject} timepoint: '${curr_suffix}'] :\t"

    # check if already submitted
    if [ "x$(qselect -N ${JOB_PREFIX}-long-${subject}-${curr_suffix})" != "x" ] ; then
      info -n "stage3 in progress, skipping"
      continue
    fi

    # check for faulty template and not running
    logfile="${SUB_PREFIX}${subject}base/scripts/recon-all.log"
    if [ -e ${logfile} ] && [ "x$(qselect -N ${JOB_PREFIX}-template-${subject})" = "x" ] ; then
      if [ "x$(tail -n1 ${logfile} | grep 'finished without')" = "x" ] ; then
        warn -n "stage2 is incomplete/faulty and not running, skipping"
        continue
      fi
    fi

    # check if template is missing and not running
    if [ ! -e ${logfile} ] && [ "x$(qselect -N ${JOB_PREFIX}-template-${subject})" = "x" ]; then
      warn -n "stage2 is missing and not yet submitted, skipping"
      continue
    fi

    # check if cross-sectional/stage1 is missing and not running
    logfile="${curr_subj}/scripts/recon-all.log"
    if [ ! -e ${logfile} ] && [ "x$(qselect -N ${JOB_PREFIX}-base-${curr_subj})" = "x" ]; then
      warn -n "stage1 is missing and not yet submitted, skipping"
      continue
    fi

    # check if already done ..
    logfile=${SUB_PREFIX}${subject}${curr_suffix}".long."${SUB_PREFIX}${subject}"base/scripts/recon-all.log"
    if [ -e ${logfile} ] ; then
      if [ "x$(tail -n1 ${logfile} | grep 'finished without')" != "x" ]; then
        cheer -n "stage3 already finished without errors"
        continue
      else
        warn -n "stage3 present and faulty, resubmitting to try again"
        rm -rf ${SUB_PREFIX}${subject}${curr_suffix}".long."${SUB_PREFIX}${subject}"base/"
      fi
    else
	  cheer -n "submitting"
    fi

    # generate dependencies and submit
    sub_deps=$(qselect -N ${JOB_PREFIX}-template-${subject})

    # meta-data
    echo "#PBS -N ${JOB_PREFIX}-long-${subject}-${curr_suffix}" >> jobfile
    echo "#PBS -l walltime=24:00:00" >> jobfile
    echo "#PBS -m n" >> jobfile
    # only add dependency line, when ther are actual dependencies, otherwise qsub
    # will fail
    if [ "x$sub_deps" != "x" ] ; then
        echo "#PBS -W depend=afterany:${sub_deps}" >> jobfile
    fi

    # actual commands
    echo 'cd $PBS_O_WORKDIR' >> jobfile
    echo "export FREESURFER_HOME=$FREESURFER_HOME" >> jobfile
    echo 'source $FREESURFER_HOME/SetUpFreeSurfer.sh' >> jobfile
    echo "export SUBJECTS_DIR='./'" >> jobfile
    echo "recon-all -long ${SUB_PREFIX}${subject}${curr_suffix} ${SUB_PREFIX}${subject}base -all" >> jobfile

    # submit and clean up tmp jobfile
    qsub jobfile >/dev/null
    rm jobfile
  done
done

echo -e "\n"    #newline
}
