#!/bin/bash

# generates jobs to create base template and submits them
#  + checks if all timepoint data are present
#  + checks if template has already been done

function stage2 {

echo "------------------------------------"
echo " Submitting Jobs for template stage"
echo "------------------------------------"


# iterate subjects
for subject in ${SUBJECTS}; do
  curr_subj=${SUB_PREFIX}${subject}
  echo -ne "\n[subject: ${curr_subj}] :\t"

  # check for running/queued jobs
  #if [[ "x$(qselect -N ${JOB_PREFIX}-base-${subject})" = "x" && $? -eq 0 ]] ; then
  if [ "x$(qselect -N ${JOB_PREFIX}-template-${subject})" != "x" ] ; then
    info -n "stage2 in progress, skipping"
    continue
  fi

  # check for (all) reference timepoints
  qsub_cmd_tps=
  count=0
  for tp in $(seq 1 $TP_COUNT); do
    curr_suffix=$(eval echo \${TP_SUFFIX_$tp})
    logfile=${curr_subj}${curr_suffix}"/scripts/recon-all.log"
    if [ "x$(qselect -N ${JOB_PREFIX}-base-${SUB_PREFIX}${subject}${curr_suffix})" = "x" ]; then
      if [ -e ${logfile} ] && [ "x$(tail -n1 ${logfile} | grep 'finished without')" == "x" ]; then
        warn -ne "timepoint '${curr_suffix}' is faulty, skipping"
        continue 2
      elif [ ! -e ${logfile} ] && [ $IGNORE_MISSING -eq 0 ]; then
	    warn -ne "timepoint '${curr_suffix}' is missing, skipping"
        continue 2
      elif [ ! -e ${logfile} ] && [ $IGNORE_MISSING -eq 1 ]; then
        continue 1
      fi
    fi
    # tp is available and is used to create the template
    count=$((count + 1))
    qsub_cmd_tps=${qsub_cmd_tps}" -tp "${curr_subj}${curr_suffix}
  done

  submitnotice=0
  # check for previous base runs and identify their status
  logfile=${SUB_PREFIX}${subject}"base/scripts/recon-all.log"
  if [ -e ${logfile} ]; then
    if [ "x$(tail -n1 ${logfile} | grep 'finished without')" != "x" ] ; then
      cheer -n "stage2 already finished without errors"
      continue
    fi
    if [ -d ${SUB_PREFIX}${subject}"base" ]; then
      warn -n "stage2 present and faulty, resubmitting to try again"
      submitnotice=1
      rm -rf ${SUB_PREFIX}${subject}"base/"
    fi
  fi

  # create recon-all call dynamically from every timepoint suffix
  # and generate dependencies
  qsub_cmd="recon-all -base ${SUB_PREFIX}${subject}base"
  for tp in $(seq 1 ${TP_COUNT}); do
    curr_suffix=$(eval echo \${TP_SUFFIX_$tp})
    # only add dependencies when they are still running
    # we don't distinguish missing and finished here
    tmp=$(qselect -N ${JOB_PREFIX}-base-${SUB_PREFIX}${subject}${curr_suffix} )
    if [ "x$tmp" != "x" ] ; then
      subj_deps=${subj_deps}":"$tmp
    fi
  done
  qsub_cmd=${qsub_cmd}" -all "${qsub_cmd_tps}

  # meta-data
  echo "#PBS -N ${JOB_PREFIX}-template-${subject}" >> jobfile
  echo "#PBS -l walltime=30:00:00" >> jobfile
  echo "#PBS -m n" >> jobfile
  # only add dependency line, when ther are actual dependencies, otherwise qsub
  # will fail
  if [ "x$subj_deps" != "x" ] ; then
      echo "#PBS -W depend=afterany${subj_deps}" >> jobfile
  fi

  # actual commands
  echo 'cd $PBS_O_WORKDIR' >> jobfile
  echo "export FREESURFER_HOME=$FREESURFER_HOME" >> jobfile
  echo 'source $FREESURFER_HOME/SetUpFreeSurfer.sh' >> jobfile
  echo "export SUBJECTS_DIR='./'" >> jobfile

  echo ${qsub_cmd} >> jobfile

  if [ $count -le 1 ] ; then
    warn -n "there are only $count timepoints, skipping"
  else
    if [ $submitnotice -eq 0 ] ; then
      cheer -n "submitting"
    fi
    # submit and clean up tmp jobfile
    qsub jobfile > /dev/null
  fi
  rm jobfile
done

echo -e "\n"    #newline
}
