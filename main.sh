#!/bin/bash

##### CHANGEME for each study according to subject file layout
# unique and minimal part of the subject id, may contain gaps
# for example $(seq 1 32) == 1 2 ... 32 
# or when padded: $(seq -w 1 32) == 01 02 ... 32
#SUBJECTS=$(seq -w 1 015)
SUBJECTS=
#SUB_PREFIX="1" # prefix every id -> 1001 1002 ... 1015
SUB_PREFIX=""
# number of timepoints and one variable for each suffix
# this is a sad limitation of the language..
#TP_COUNT=3
#TP_SUFFIX_1="_v1"
#TP_SUFFIX_2="_v2"
#TP_SUFFIX_3="_v3"
TP_COUNT=2
TP_SUFFIX_1=
TP_SUFFIX_2=
# complete image affixes to match raw images
#IMG_PREFIX="STUDY_"
IMG_PREFIX=""
#IMG_SUFFIX="_T1.img"
IMG_SUFFIX=""
# all the above will create image names of the form:
# STUDY_1010_v2_T1.img
# FIXME: figure out what to do when tps are in front of the id
#
# wether we want to ignore missing timepoints
IGNORE_MISSING=0
# set the version and location of the freesurfer distribution
FREESURFER_HOME=/opt/freesurfer/6.0.0
# set the location of the script helper libraries
# source: https://git.mpib-berlin.mpg.de/krause/freesurfer-longitudinal-pipeline
LIBDIR="$PWD"
##############################################################


# GO !

if [ x"$SUBJECTS" = "x" ] || [ -f $LIBDIR/common.sh ] ; then
    echo "$0 ist not configured, please edit"
    exit 1
fi

source $LIBDIR/common.sh
source $LIBDIR/stage1.sh
source $LIBDIR/stage2.sh
source $LIBDIR/stage3.sh

stage1
sleep 5s
stage2
sleep 5s
stage3

exit 0
