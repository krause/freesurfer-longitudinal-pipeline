#!/bin/bash

# this is the fixed (initially random) name prefix for the jobfiles so we can
# isolate running dependencies
if [ -f .JOB_PREFIX ] ; then
    JOB_PREFIX=$(cat .JOB_PREFIX)
else
    JOB_PREFIX=$(dd if=/dev/random bs=1 count=3 2>/dev/null |base64)
    echo $JOB_PREFIX > .JOB_PREFIX
fi

function warn() {
    echo -ne '\e[0;31m' #red
    echo $*
    echo -ne '\e[0m'
}

function info() {
    echo -ne '\e[0;33m' #yellow
    echo $*
    echo -ne '\e[0m'
}

function cheer() {
    echo -ne '\e[0;32m' #green
    echo $*
    echo -ne '\e[0m'
}
