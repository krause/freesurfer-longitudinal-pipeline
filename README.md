## quick start

### [Freesurfer Reference](http://surfer.nmr.mgh.harvard.edu/fswiki/recon-all)

### What we want to do:

For a list of images with the following format:

     PREFIX ### ID_PREFIX ### ID ### TIMEPOINT ### SUFFIX

For example: `STUDY_subj208_v1_T1.nii` would map to:

    PREFIX=STUDY_
    ID_PREFIX=subj
    ID=208
    TIMEPOINT=_v1
    SUFFIX=_T1.nii

we want to run the following steps:

#### 1. Cross-Sectional analysis for each subject
#### if there are multiple timepoints:
#### 2. Create a template from all timepoints
#### 3. Longitudinal analysis for each subject against the template


When everything is done, all you need to do is 

+ copy your nifits into a directory
+ add a fresh copy of main.sh to the folder
+ edit main.sh according to your study layout
+ run main.sh


### Example

#### 0. check-out repo (if not done already)

```bash
[krause@node1 ~] mkdir scripts && cd scripts
[krause@node1 ~/scripts] git clone https://git.mpib-berlin.mpg.de/krause/freesurfer-longitudinal-pipeline
```

#### 1. sync repo

```bash
[krause@node1 ~] cd scripts/freesurfer-longitudinal-pipeline/
[krause@node1 ~/scripts/freesurfer-longitudinal-pipeline (master)] git pull
Already up-to-date.
[krause@node1 ~/scripts/freesurfer-longitudinal-pipeline (master)]
```


#### 2. copy and edit `main.sh` for a new study (SUBJECTS, AFFIXES, LIBDIR, ...)

```bash
[krause@node1 ~] cd study/
[krause@node1 ~/study] cp ../scripts/freesurfer-longitudinal-pipeline/main.sh .
[krause@node1 ~/study] nano main.sh
```

#### 3. fire up main.sh

```bash
[krause@node1 ~/study] ./main.sh
```

If you get a permission denied error, use `bash main.sh` or set executable bit with `chmod +x main.sh` before starting.
